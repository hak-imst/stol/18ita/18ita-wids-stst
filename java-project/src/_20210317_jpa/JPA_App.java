package _20210317_jpa;


import java.util.GregorianCalendar;

import _20210317_jpa.entities.Order;
import _20210317_jpa.entities.OrderPositions;
import _20210317_jpa.entities.Product;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class JPA_App {
    public static void main(String[] args) {
        // Setup the entity manager
        EntityManagerFactory factory =   Persistence.createEntityManagerFactory("mysqlUnit");
        EntityManager em = factory.createEntityManager();
        EntityTransaction trans = em.getTransaction();

        trans.begin();

        Order order1 = new Order();
        order1.setOrder_date(new GregorianCalendar(2021,03,17));

        em.persist(order1);

        System.out.println(order1);

        Product prod1 = new Product();
        prod1.setName("Hammer");

        em.persist(prod1);

        System.out.println(prod1);

        Product prod2 = new Product();
        prod2.setName("Nagel");

        em.persist(prod2);

        OrderPositions pos1 = new OrderPositions();
        pos1.setAmount(10);
        pos1.setOrder(order1);
        pos1.setProduct(prod1);

        em.persist(pos1);

        OrderPositions pos2 = new OrderPositions();
        pos2.setAmount(15);
        pos2.setOrder(order1);    
        pos2.setProduct(prod2);    

        em.persist(pos2);
        
        trans.commit();

        System.out.println(order1);

        Order getOrder = em.find(Order.class, order1.getId());
        System.out.println(getOrder);
        
        // Close the entity manager
        em.close();
        factory.close();
    }
}
