package _20210317_jpa.entities;

import java.util.GregorianCalendar;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Orders")
public class Order {

    @Id @GeneratedValue
    private int id;

    GregorianCalendar order_date;

    @OneToMany( targetEntity=OrderPositions.class, cascade=CascadeType.ALL, mappedBy="order" )
    private List<OrderPositions> positionlist;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GregorianCalendar getOrder_date() {
        return order_date;
    }

    public void setOrder_date(GregorianCalendar order_date) {
        this.order_date = order_date;
    }

    public List<OrderPositions> getPositionlist() {
        return positionlist;
    }

    public void setPositionlist(List<OrderPositions> positionlist) {
        this.positionlist = positionlist;
    }

    @Override
    public String toString() {
        return "Order [id=" + id + ", order_date=" + order_date.getTimeInMillis() + ", positionlist=" + positionlist + "]";
    }

    
    
    
    
}
