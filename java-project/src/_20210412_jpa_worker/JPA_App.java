package _20210412_jpa_worker;

import _20210412_jpa_worker.entities.Arbeiter;
import _20210412_jpa_worker.entities.Manager;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class JPA_App {
    public static void main(String[] args) {
        // Setup the entity manager
        EntityManagerFactory factory =   Persistence.createEntityManagerFactory("mysqlUnit");
        EntityManager em = factory.createEntityManager();

        Manager m1 = new Manager();
        m1.setGehalt(100);
        m1.setBeteiligung(10);

        Arbeiter a1 = new Arbeiter();
        a1.setGehalt(70);
        a1.setZuschuss(5);       

        // Add it
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(m1);
        em.persist(a1);
        trans.commit();
        
        // Close the entity manager
        em.close();
        factory.close();
    }
}
