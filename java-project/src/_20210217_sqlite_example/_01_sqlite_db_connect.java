package _20210217_sqlite_example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class _01_sqlite_db_connect {
    public static void main(String[] args) {
        System.out.println("Connecting to db ...");
        DbWork.connect();
    }
}

class DbWork {
    public static void connect() {
        Connection conn = null;
        String url = "jdbc:sqlite:./src/_20210217_sqlite_example/example.db";

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if(conn != null) {
                    conn.close();
                }                
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
