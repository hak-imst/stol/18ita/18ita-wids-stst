package _20210310_jpa_inheritance;


import _20210310_jpa_inheritance.entities.Car;
import _20210310_jpa_inheritance.entities.MotorCycle;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class JPA_App {
    public static void main(String[] args) {
        // Setup the entity manager
        EntityManagerFactory factory =   Persistence.createEntityManagerFactory("mysqlUnit");
        EntityManager em = factory.createEntityManager();

        Car car = new Car();
        car.setName("TESLA Roadster");
        car.setSpeed(402);
        car.setTires(4);
        car.setRoofType("Glass");

        MotorCycle mc = new MotorCycle();
        mc.setName("HONDA Goldwing");
        mc.setSpeed(200);
        mc.setTires(2);
        mc.setMcSpecific("Mit Radio");

        // Add it
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(car);
        em.persist(mc);
        trans.commit();
        
        // Close the entity manager
        em.close();
        factory.close();
    }
}
