package _20210310_jpa_inheritance.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class MotorCycle extends Vehicle {

    @Id @GeneratedValue
    private int id;

    private String mcSpecific;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMcSpecific() {
        return mcSpecific;
    }

    public void setMcSpecific(String mcSpecific) {
        this.mcSpecific = mcSpecific;
    }

    
    
}
