/* https://www.mysqltutorial.org/mysql-regular-expression-regexp.aspx */

USE mysqltutorial;



SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^(A|B|C)'
ORDER BY productname;

SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^(A|B|C)m'
ORDER BY productname;

SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^(Am|B|C)'
ORDER BY productname;

SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '.*'
ORDER BY productname
LIMIT 10;

SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^[0-9]'
ORDER BY productname
LIMIT 10;

SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^[0-9]{3,3}[^47]'
ORDER BY productname
LIMIT 10;

SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP 'ne$'
ORDER BY productname
LIMIT 10;