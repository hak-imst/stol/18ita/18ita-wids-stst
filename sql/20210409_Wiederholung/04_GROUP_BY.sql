use w3c;

DESCRIBE orders;
DESCRIBE orderdetails;
DESCRIBE products;


SELECT orders.OrderID as BestNr, orderdetails.Quantity, products.ProductName, products.Price * orderdetails.Quantity  as Netto, products.Price * orderdetails.Quantity * 1.2 as Brutto
    FROM orders
    LEFT JOIN orderdetails ON orders.OrderID = orderdetails.OrderID
    LEFT JOIN products ON products.ProductID = orderdetails.ProductID
    LIMIT 10;

SELECT orders.OrderID as BestNr, COUNT(*) as Positionen,SUM(orderdetails.Quantity) as "Stückzahl", SUM(products.Price * orderdetails.Quantity)  as Netto, SUM(products.Price * orderdetails.Quantity * 1.2) as Brutto
    FROM orders
    LEFT JOIN orderdetails ON orders.OrderID = orderdetails.OrderID
    LEFT JOIN products ON products.ProductID = orderdetails.ProductID
    GROUP BY orders.OrderID
    HAVING Positionen > 2
    LIMIT 10;