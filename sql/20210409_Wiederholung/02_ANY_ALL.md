* https://www.javatpoint.com/mysql-any
* https://dev.mysql.com/doc/refman/8.0/en/any-in-some-subqueries.html
  
The ANY keyword, which must follow a comparison operator, means “return TRUE if the comparison is TRUE for ANY of the values in the column that the subquery returns.” For example:

```mysql
SELECT s1 FROM t1 WHERE s1 > ANY (SELECT s1 FROM t2);
```

When used with a subquery, the word IN is an alias for = ANY. Thus, these two statements are the same:

```mysql
SELECT s1 FROM t1 WHERE s1 = ANY (SELECT s1 FROM t2);
SELECT s1 FROM t1 WHERE s1 IN    (SELECT s1 FROM t2);
```

* https://dev.mysql.com/doc/refman/8.0/en/all-subqueries.html

The word ALL, which must follow a comparison operator, means “return TRUE if the comparison is TRUE for ALL of the values in the column that the subquery returns.” For example:

```mysql
SELECT s1 FROM t1 WHERE s1 > ALL (SELECT s1 FROM t2);
```