use w3c;

DESCRIBE orders;

SELECT orders.OrderID, orders.OrderDate
    FROM orders
    ORDER BY orders.OrderDate ASC;

SELECT orders.OrderID, orders.OrderDate
    FROM orders
    WHERE orders.OrderDate > '1997-01-30'
    ORDER BY orders.OrderDate ASC;

SELECT orders.OrderID, orders.OrderDate, ADDDATE(orders.OrderDate, INTERVAL 1 YEAR)
    FROM orders
    WHERE DATEDIFF("1997-02-12", orders.OrderDate) < 5
    ORDER BY orders.OrderDate ASC;