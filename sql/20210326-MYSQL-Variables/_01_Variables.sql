USE mysqltutorial;

SET @counter := 100;

SELECT 
    productCode, productName, productLine, msrp
FROM
    products
LIMIT 10;


SELECT 
    @msrp:=MAX(msrp),
    @msrp2:=136.00
FROM
    products;


SELECT 
    productCode, productName, productLine, msrp, @counter, 150, msrp * 1.2 as BruttoPreis
FROM
    products
WHERE
    msrp = @msrp2 OR msrp = @msrp;

